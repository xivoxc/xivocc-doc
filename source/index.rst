.. xivo-cc-doc Documentation master file.

*********************
XiVO-CC Documentation
*********************

.. figure:: logo_xivo-cc.png
   :scale: 30%


Documentation has now moved to https://documentation.xivo.solutions/
